# Orange

API Gateway based on OpenResty.


### Documents

Find all about **Orange** from [Documents Website](http://orange.sumory.com/docs).


### See also

The architecture is highly inspired by [Kong](https://github.com/Mashape/kong).